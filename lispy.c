#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <math.h>

#include <editline/readline.h>

#define LASSERT(args, cond, fmt, ...) \
  if (!(cond)) { \
    lval* err = lval_err(fmt, ##__VA_ARGS__); \
    lval_del(args); \
    return err; \
  }

#define LASSERT_ARGT(func, args, index, expect) \
  LASSERT(args, args->cell[index]->type == expect, \
    "Function '%s' passed incorrect type for argument %i. " \
    "Got %s, Expected %s.", \
    func, index, \
    ltype_name(args->cell[index]->type), ltype_name(expect))

#define LASSERT_ARGC(func, args, num) \
  LASSERT(args, args->count == num, \
    "Function '%s' passed incorrect number of arguments. " \
    "Got %i, Expected %i.", \
    func, args->count, num)

#define LASSERT_ARGC_MIN(func, args, num) \
  LASSERT(args, args->count >= num, \
    "Function '%s' must have at least %i arguments. " \
    "Got %i.", \
    func, num, args->count)

#define LASSERT_ARGC_MAX(func, args, num) \
  LASSERT(args, args->count <= num, \
    "Function '%s' passed too many arguments. " \
    "Got %i, Expected %i", \
    func, args->count, num)

#define LASSERT_ARGE(func, args, index) \
  LASSERT(args, args->cell[index]->count == 0, \
    "Function '%s' need to be called without arguments.", \
    func)

#define LASSERT_ARGNE(func, args, index) \
  LASSERT(args, args->cell[index]->count != 0, \
    "Function '%s' passed {} for argument %i.", \
    func, index)

// child
#define LASSERT_PARAM(args, param, cond, fmt, ...) \
  if (!(cond)) { \
    lval* err = lval_err(fmt, ##__VA_ARGS__); \
    lval_del(param); \
    lval_del(args); \
    return err; \
  }

#define LASSERT_PARAM_ARGC(func, param, args, child, num) \
  LASSERT_PARAM(args, child, child->count == num, \
    "Param '%s' of " \
    "Function '%s' passed incorrect number of arguments. " \
    "Got %i, Expected %i.", \
    param, func, child->count, num); \

#define LASSERT_PARAM_ARGC_MIN(func, param, args, child, num) \
  LASSERT_PARAM(args, child, child->count >= num, \
    "Param '%s' of " \
    "Function '%s' must have at least %i arguments. " \
    "Got %i.", \
    param, func, num, child->count); \

#define LASSERT_PARAM_ARGC_MAX(func, param, args, child, num) \
  LASSERT_PARAM(args, child, child->count <= num, \
    "Param '%s' of " \
    "Function '%s' passed too many arguments. " \
    "Got %i, Expected %i", \
    param, func, child->count, num); \

// forward declarations
struct lval;
struct lenv;
typedef struct lval lval;
typedef struct lenv lenv;

// lisp value
enum { LVAL_ERR, LVAL_NUM,   LVAL_SYM,   LVAL_STR,
       LVAL_FUN, LVAL_SEXPR, LVAL_QEXPR };

typedef lval*(*lbuiltin)(lenv*, lval*);

struct lval {
  int type;

  // basic
  double num;
  char* err;
  char* sym;
  char* str;

  // function
  lbuiltin builtin;
  lenv* env;
  lval* formals;
  lval* body;

  // expression
  int count;
  lval** cell;
};

struct lenv {
  lenv* par;
  int count;
  char** syms;
  lval** vals;
};

// function forward definition
void lval_print(lval* v);
lval* lval_eval(lenv* e, lval* v);

lenv* lenv_new(void);
void lenv_del(lenv* e);
lval* lenv_get(lenv* e, lval* k);
void lenv_put(lenv* e, lval* k, lval* v);
lenv* lenv_copy(lenv* s);
void lenv_def(lenv* e, lval* k, lval* v);
lval* lval_add(lval* v, lval* x);

lval* builtin_eval(lenv* e, lval* a);
lval* builtin_list(lenv* e, lval* a);

// HELPERS
// type name
char* ltype_name(int t) {
  switch (t) {
    case LVAL_FUN: return "Function";
    case LVAL_NUM: return "Number";
    case LVAL_ERR: return "Error";
    case LVAL_SYM: return "Symbol";
    case LVAL_STR: return "String";
    case LVAL_SEXPR: return "S-Expression";
    case LVAL_QEXPR: return "Q-Expression";
    default: return "Unknown";
  }
}

// CONSTRUCTORS
// constructor new number lval
lval* lval_num(double x) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_NUM;
  v->num = x;
  return v;
}

// constructor new error lval
lval* lval_err(char* fmt, ...) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_ERR;

  // create a va list and initialize it
  va_list va;
  va_start(va, fmt);

  // allocate 512 bytes of space
  v->err = malloc(512);

  // printf the error string with maximum of 511 characteres
  vsnprintf(v->err, 511, fmt, va);

  // reallocace to number of bytes actually used
  v->err = realloc(v->err, strlen(v->err) + 1);

  // cleanup
  va_end(va);

  return v;
}

// constructor new symbol lval
lval* lval_sym(char* s) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_SYM;
  v->sym = malloc(strlen(s) + 1);
  strcpy(v->sym, s);
  return v;
}

// constructor new str
lval* lval_str(char* s) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_STR;
  v->str = malloc(strlen(s) + 1);
  strcpy(v->str, s);
  return v;
}

// constructor new fun lval
lval* lval_fun(lbuiltin func) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_FUN;
  v->builtin = func;
  return v;
}

// constructor new lambda
lval* lval_lambda(lval* formals, lval* body) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_FUN;

  // set builtin to null
  v->builtin = NULL;

  // build new environment
  v->env = lenv_new();

  // set formals and body
  v->formals = formals;
  v->body = body;

  return v;
}

// pointer to a new empty Sexpr lval
lval* lval_sexpr(void) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_SEXPR;
  v->count = 0;
  v->cell = NULL;
  return v;
}

// pointer to a new empty Qexpr lval
lval* lval_qexpr(void) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_QEXPR;
  v->count = 0;
  v->cell = NULL;
  return v;
}

lval* lval_swrap(lval* expr) {
  lval* w = lval_sexpr();
  lval_add(w, expr);

  return w;
}

lval* lval_qwrap(lval* expr) {
  lval* w = lval_qexpr();
  lval_add(w, expr);

  return w;
}

// free
void lval_del(lval* v) {
  switch (v->type) {
    case LVAL_NUM: break;
    case LVAL_ERR: free(v->err); break;
    case LVAL_SYM: free(v->sym); break;
    case LVAL_STR: free(v->str); break;
    case LVAL_FUN:
      if (!v->builtin) {
        lenv_del(v->env);
        lval_del(v->formals);
        lval_del(v->body);
      }
      break;
    case LVAL_SEXPR:
    case LVAL_QEXPR:
      for (int i = 0; i < v->count; i++) {
        lval_del(v->cell[i]);
      }
      free(v->cell);
    break;
  }

  free(v);
}

// add new lval
lval* lval_add(lval* v, lval* x) {
  v->count++;
  v->cell = realloc(v->cell, sizeof(lval *) * v->count);
  v->cell[v->count - 1] = x;
  return v;
}

char* lang_number = "-0123456789.";
char* lang_math = "-+*/%";
char* lang_relational = "=<>!";
char* lang_special = "\\&";
char* lang_symbol =
  "abcdefghijklmnopqrstuvwxyz"
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ_";

char* cat(int count, ...) {
  char* part = calloc(1, 1);
  va_list va;
  va_start(va, count);
  for (int i = 0; i < count; i++) {
    char *val = va_arg(va, char *);
    while(*val) {
      part = realloc(part, strlen(part) + 2);
      part[strlen(part) + 1] = '\0';
      part[strlen(part) + 0] = *val;
      val++;
    }
  }
  va_end(va);
  return part;
}

char* lang(void) {
  return cat(5, lang_symbol, lang_number, lang_math,
                lang_relational, lang_special);
}

char* lval_str_unescapable = "abfnrtv\\\'\"";
char* lval_str_escapable = "\a\b\f\n\r\t\v\\\'\"";

char lval_str_unescape(char x) {
  switch (x) {
    case 'a': return '\a';
    case 'b': return '\b';
    case 'f': return '\f';
    case 'n': return '\n';
    case 'r': return '\r';
    case 't': return '\t';
    case 'v': return '\v';
    case '\\': return '\\';
    case '\'': return '\'';
    case '\"': return '\"';
  }
  return '\0';
}

char* lval_str_escape(char x) {
  switch (x) {
    case '\a': return "\\a";
    case '\b': return "\\b";
    case '\f': return "\\f";
    case '\n': return "\\n";
    case '\r': return "\\r";
    case '\t': return "\\t";
    case '\v': return "\\v";
    case '\\': return "\\\\";
    case '\'': return "\\\'";
    case '\"': return "\\\"";
  }
  return "";
}

lval* lval_read(char* s, int* i);

lval* lval_parse(char* s, int* i, char end) {
  lval* x = (end == '}') ? lval_qexpr() : lval_sexpr();

  while (s[*i] != end) {
    lval* y = lval_read(s, i);

    if (y->type == LVAL_ERR) {
      lval_del(x);
      return y;
    } else {
      lval_add(x, y);
    }
  }
  (*i)++;

  return x;
}

// void lval_println(lval* v);
lval* lval_read_sym(char* s, int* i);
lval* lval_read_str(char* s, int* i);

lval* lval_read(char* s, int* i) {
  lval* x = NULL;

  // whitespace
  while (strchr(" \t\r\v\n;", s[*i]) && s[*i] != '\0') {
    if (s[*i] == ';') {
      while (s[*i] != '\n' && s[*i] != '\0') { (*i)++; }
    }
    (*i)++;
  }

  // end
  if (s[*i] == '\0') {
    return lval_err("Unexpected end of input");
  }

  // S-Expr
  else if (s[*i] == '(') {
    (*i)++;
    x = lval_parse(s, i, ')');
  }

  // Q-Expr
  else if (s[*i] == '{') {
    (*i)++;
    x = lval_parse(s, i, '}');
  }

  // Sym
  else if (strchr(lang(), s[*i])) {
    x = lval_read_sym(s, i);
  }

  // text
  else if (strchr("\"", s[*i])) {
    x = lval_read_str(s, i);
  }

  else {
    x = lval_err("Unknown Character %c", s[*i]);
  }

  // whitespace
  while (strchr(" \t\r\v\n;", s[*i]) && s[*i] != '\0') {
    if (s[*i] == ';') {
      while (s[*i] != '\n' && s[*i] != '\0') { (*i)++; }
    }
    (*i)++;
  }

  return x;
}

lval* lval_read_str(char* s, int* i) {
  // allocate empty string
  char* part = calloc(1, 1);

  (*i)++;
  while (s[*i] != '"') {
    char c = s[*i];

    if (c == '\0') {
      free(part);
      return lval_err("Unexpected end of input at String literal.");
    }

    // escape
    if (c == '\\') {
      (*i)++;
      if (strchr(lval_str_unescapable, s[*i])) {
        c = lval_str_unescape(s[*i]);
      } else {
        free(part);
        return lval_err("Invalid escape character \\%c", s[*i]);
      }
    }

    // append character to string
    part = realloc(part, strlen(part) + 2);
    part[strlen(part) + 1] = '\0';
    part[strlen(part) + 0] = c;

    (*i)++;
  }
  (*i)++;

  lval* x = lval_str(part);

  // clean
  free(part);

  // return String
  return x;
}

lval* lval_read_sym(char* s, int* i) {
  // allocate empty string
  char* part = calloc(1, 1);

  // while valid identifier characters
  while (strchr(lang(), s[*i]) && s[*i] != '\0') {

    // apend character to end of string
    part = realloc(part, strlen(part) + 2);
    part[strlen(part) + 1] = '\0';
    part[strlen(part) + 0] = s[*i];
    (*i)++;
  }

  // check if identifier looks like number
  int is_num = strchr(lang_number, part[0]) != NULL;
  for (int j = 1; j < strlen(part); j++) {
    if (strchr("0123456789.", part[j]) == NULL) { is_num = 0; break; }
  }
  if (strlen(part) == 1 && part[0] == '-') { is_num = 0; }

  // add symbol or number as lval
  lval* x = NULL;
  if (is_num) {
    errno = 0;
    double v = strtod(part, NULL);
    x = (errno != ERANGE) ? lval_num(v) : lval_err("Invalid Number %s", part);
  } else {
    x = lval_sym(part);
  }

  // free temp string
  free(part);

  // return new position
  return x;
}

void lval_expr_print(lval* v, char open, char close) {
  putchar(open);
  for (int i = 0; i < v->count; i++) {
    lval_print(v->cell[i]);
    if (i != (v->count - 1)) {
      putchar(' ');
    }
  }
  putchar(close);
}

void lval_print_str(lval* v) {
  putchar('"');
  // loop over the characters in the string
  for (int i = 0; i < strlen(v->str); i++) {
    if (strchr(lval_str_escapable, v->str[i])) {
      printf("%s", lval_str_escape(v->str[i]));
    } else {
      putchar(v->str[i]);
    }
  }
  putchar('"');
}

void lval_print(lval* v) {
  switch (v->type) {
    case LVAL_NUM: printf("%f", v->num); break;
    case LVAL_ERR: printf("Error: %s", v->err); break;
    case LVAL_SYM: printf("%s", v->sym); break;
    case LVAL_STR: lval_print_str(v); break;
    case LVAL_FUN:
      if (v->builtin) {
        printf("<function>");
      } else {
        printf("\\ "); lval_print(v->formals);
        putchar(' '); lval_print(v->body); putchar(')');
      }
      break;
    case LVAL_SEXPR: lval_expr_print(v, '(', ')'); break;
    case LVAL_QEXPR: lval_expr_print(v, '{', '}'); break;
  }
}

void lval_println(lval* v) {
  lval_print(v);
  putchar('\n');
}

// HEAP
// pop
lval* lval_pop(lval* v, int i) {
  // find the item at i
  lval* x = v->cell[i];

  // shift memory after the item at i over the top
  memmove(&v->cell[i], &v->cell[i + 1],
    sizeof(lval*) * (v->count - i - 1));

  // decrease the count of items in the list
  v->count--;

  // reallocate the memory used
  v->cell = realloc(v->cell, sizeof(lval*) * v->count);

  return x;
}

// take
lval* lval_take(lval* v, int i) {
  lval* x = lval_pop(v, i);
  lval_del(v);
  return x;
}

// join
lval* lval_join(lval* x, lval* y) {
  // for each cell in y add it to x
  while (y->count) {
    x = lval_add(x, lval_pop(y, 0));
  }

  // delete empty y and return x
  lval_del(y);
  return x;
}

// copy
lval* lval_copy(lval* v) {
  lval* x = malloc(sizeof(lval));
  x->type = v->type;

  switch (v->type) {
    // copy functions and numbers
    case LVAL_FUN:
      if (v->builtin) {
        x->builtin = v->builtin;
      } else {
        x->builtin = NULL;
        x->env = lenv_copy(v->env);
        x->formals = lval_copy(v->formals);
        x->body = lval_copy(v->body);
      }
      break;
    case LVAL_NUM: x->num = v->num; break;

    // copy strings using malloc and strcpy
    case LVAL_ERR:
      x->err = malloc(strlen(v->err) + 1);
      strcpy(x->err, v->err); break;

    case LVAL_SYM:
      x->sym = malloc(strlen(v->sym) + 1);
      strcpy(x->sym, v->sym); break;

    case LVAL_STR:
      x->str = malloc(strlen(v->str) + 1);
      strcpy(x->str, v->str); break;

    // copy list by copying ecah sub-expression
    case LVAL_SEXPR:
    case LVAL_QEXPR:
      x->count = v->count;
      x->cell = malloc(sizeof(lval*) * x->count);
      for (int i = 0; i < x->count; i++) {
        x->cell[i] = lval_copy(v->cell[i]);
      }
      break;
  }

  return x;
}

int lval_eq(lval* x, lval* y) {
  // diferent types are always unequal
  if (x->type != y->type) { return 0; }

  // compare type
  switch (x->type) {
    // number
    case LVAL_NUM: return (x->num == y->num);

    // strings
    case LVAL_ERR: return (strcmp(x->err, y->err) == 0);
    case LVAL_SYM: return (strcmp(x->sym, y->sym) == 0);
    case LVAL_STR: return (strcmp(x->str, y->str) == 0);

    // functions
    case LVAL_FUN:
      if (x->builtin || y->builtin) {
        return x->builtin == y->builtin;
      } else {
        return lval_eq(x->formals, y->formals)
          && lval_eq(x->body, y->body);
      }

    // list
    case LVAL_QEXPR:
    case LVAL_SEXPR:
      if (x->count != y->count) { return 0; }
      for (int i = 0; i < x->count; i++) {
        if (!lval_eq(x->cell[i], y->cell[i])) { return 0; }
      }
      return 1;
  }
  return 0;
}

// EVAL
lval* lval_call(lenv* e, lval* f, lval* a) {
  // if builtin just execute it
  if (f->builtin) { return f->builtin(e, a); }

  // record argc
  int given = a->count;
  int total = f->formals->count;

  // while args still remain to be processed
  while (a->count) {
    // if we've ran out of formal arguments to bind
    if (f->formals->count == 0) {
      lval_del(a); return lval_err(
        "Function passed too many arguments. "
        "Got %i, Expected %i.", given, total);
    }

    // pop first symbol from formals
    lval* sym = lval_pop(f->formals, 0);

    // special case to deal with '&'
    if (strcmp(sym->sym, "&") == 0) {
      // ensure '&' is followed by another symbol
      if (f->formals->count != 1) {
        lval_del(a);
        return lval_err("Function format invalid. "
          "Symbol '&' not followed by syngle symbol.");
      }

      // next formal should be bound to remaining arguments
      lval* nsym = lval_pop(f->formals, 0);
      lenv_put(f->env, nsym, builtin_list(e, a));
      lval_del(sym); lval_del(nsym);
      break;
    }

    // pop next argument from the list
    lval* val = lval_pop(a, 0);

    // bind a copy into the function's environment
    lenv_put(f->env, sym, val);

    // delete symbol and value
    lval_del(sym); lval_del(val);
  }

  // argument list is now bound so can be cleaned up
  lval_del(a);

  // if '&' remains in formal list bind to empty list
  if (f->formals->count > 0 &&
    strcmp(f->formals->cell[0]->sym, "&") == 0) {
    // check to ensure that & is not passed invalidly
    if (f->formals->count != 2) {
      return lval_err("Function format invalid. "
        "Symbol '&' not followed by single symbol.");
    }

    // pop and delete '&' symbol
    lval_del(lval_pop(f->formals, 0));

    // pop next symbol and create empty list
    lval* sym = lval_pop(f->formals, 0);
    lval* val = lval_qexpr();

    // bind to environment and delete
    lenv_put(f->env, sym, val);
    lval_del(sym); lval_del(val);
  }

  // if all formals have been bound evaluate it
  if (f->formals->count == 0) {
    // set environment parent to evaluation environment
    f->env->par = e;

    // evaluate and return
    return builtin_eval(
      f->env, lval_add(lval_sexpr(), lval_copy(f->body)));
  } else {
    // otherwise return partially evaluated fuction
    return lval_copy(f);
  }
}

lval* lval_eval_sexpr(lenv* e, lval* v) {
  // eval childrens
  for (int i = 0; i < v->count; i++) {
    v->cell[i] = lval_eval(e, v->cell[i]);
  }

  // check errors
  for (int i = 0; i < v->count; i++) {
    if (v->cell[i]->type == LVAL_ERR) { return lval_take(v, i); }
  }

  // empty expression
  if (v->count == 0) { return v; }

  // single expression
  if (v->count == 1) { return lval_take(v, 0); }

  // ensure first element is symbol of func
  lval* f = lval_pop(v, 0);
  if (f->type != LVAL_FUN) {
    lval* err = lval_err(
      "S-Expression starts with incorrect type. "
      "Got %s, Expected %s.",
      ltype_name(f->type), ltype_name(LVAL_FUN));
    lval_del(f); lval_del(v);
    return err;
  }

  // call function to get result
  lval* result = lval_call(e, f, v);
  lval_del(f);
  return result;
}

lval* lval_eval(lenv* e, lval* v) {
  if (v->type == LVAL_SYM) {
    lval* x = lenv_get(e, v);
    lval_del(v);
    return x;
  }
  // eval sexpressions
  if (v->type == LVAL_SEXPR) { return lval_eval_sexpr(e, v); }

  // all other lval types remain the same
  return v;
}

// BUILTIN
// var
lval* builtin_var(lenv* e, lval* a, char* func) {
  LASSERT_ARGT(func, a, 0, LVAL_QEXPR);

  lval* syms = a->cell[0];
  for (int i = 0; i < syms->count; i++) {
    LASSERT(a, (syms->cell[i]->type == LVAL_SYM),
      "Function '%s' cannot define non-symbol. "
      "Got %s, Expected %s.", func,
      ltype_name(syms->cell[i]->type),
      ltype_name(LVAL_SYM));
  }

  LASSERT(a, (syms->count == a->count - 1),
    "Function '%s' passed too many arguments for symbols. "
    "Got %i, Expected %i.", func, syms->count, a->count - 1);

  for (int i = 0; i < syms->count; i++) {
    // if 'def' define globally
    if (strcmp(func, "def") == 0 ||
      strcmp(func, "fun") == 0) {
      lenv_def(e, syms->cell[i], a->cell[i + 1]);
    }

    // if 'put' define locally
    if (strcmp(func, "=") == 0) {
      lenv_put(e, syms->cell[i], a->cell[i + 1]);
    }
  }

  lval_del(a);
  return lval_sexpr();
}

// def {} {}
lval* builtin_def(lenv* e, lval* a) {
  return builtin_var(e, a, "def");
}

// = {name} value
lval* builtin_put(lenv* e, lval* a) {
  return builtin_var(e, a, "=");
}

// \ {args} {body}
lval* builtin_lambda(lenv* e, lval* a) {
  // validate
  LASSERT_ARGC("\\", a, 2);
  LASSERT_ARGT("\\", a, 0, LVAL_QEXPR);
  LASSERT_ARGT("\\", a, 1, LVAL_QEXPR);

  // check first Q-Expression contains only symbols
  for (int i = 0; i < a->cell[0]->count; i++) {
    LASSERT(a, (a->cell[0]->cell[i]->type == LVAL_SYM),
      "Cannot define non-symbol. Got %s, Expected %s.",
      ltype_name(a->cell[0]->cell[i]->type),
      ltype_name(LVAL_SYM));
  }

  // pop first two args and pass them to lval_lambda
  lval* formals = lval_pop(a, 0);
  lval* body = lval_pop(a, 0);
  lval_del(a);

  return lval_lambda(formals, body);
}

// RELATIONAL OPERATORS
// gt 1 2 3
lval* builtin_gt(lenv* e, lval* a) {
  for (int i = 0; i < a->count; i++) {
    LASSERT_ARGT("gt", a, i, LVAL_NUM);
  }

  lval* x = lval_pop(a, 0);
  while (a->count) {
    lval* y = lval_pop(a, 0);
    if (x->num > y->num) { lval_del(y); continue; }
    lval_del(a); lval_del(x);
    return lval_num(0);
  }

  lval_del(a); lval_del(x);
  return lval_num(1);
}

// gte 5 5 2 1
lval* builtin_gte(lenv* e, lval* a) {
  for (int i = 0; i < a->count; i++) {
    LASSERT_ARGT("gte", a, i, LVAL_NUM);
  }

  lval* x = lval_pop(a, 0);
  while (a->count) {
    lval* y = lval_pop(a, 0);
    if (x->num >= y->num) { lval_del(y); continue; }
    lval_del(a); lval_del(x);
    return lval_num(0);
  }

  lval_del(a); lval_del(x);
  return lval_num(1);
}

// lt 1 3 8
lval* builtin_lt(lenv* e, lval* a) {
  for (int i = 0; i < a->count; i++) {
    LASSERT_ARGT("lt", a, i, LVAL_NUM);
  }

  lval* x = lval_pop(a, 0);
  while (a->count) {
    lval* y = lval_pop(a, 0);
    if (x->num < y->num) { lval_del(y); continue; }
    lval_del(a); lval_del(x);
    return lval_num(0);
  }

  lval_del(a); lval_del(x);
  return lval_num(1);
}

// lte 2 2 1
lval* builtin_lte(lenv* e, lval* a) {
  for (int i = 0; i < a->count; i++) {
    LASSERT_ARGT("lt", a, i, LVAL_NUM);
  }

  lval* x = lval_pop(a, 0);
  while (a->count) {
    lval* y = lval_pop(a, 0);
    if (x->num <= y->num) { lval_del(y); continue; }
    lval_del(a); lval_del(x);
    return lval_num(0);
  }

  lval_del(a); lval_del(x);
  return lval_num(1);
}

// eq 1 1 1
lval* builtin_eq(lenv* e, lval* a) {
  lval* x = lval_pop(a, 0);
  while (a->count) {
    lval* y = lval_pop(a, 0);
    if (!lval_eq(x, y)) {
      lval_del(a); lval_del(x); lval_del(y);
      return lval_num(0);
    }
    lval_del(y);
  }

  lval_del(a); lval_del(x);
  return lval_num(1);
}

// MATHEMATICAL OPERATORS
// op
lval* builtin_op(lenv* e, lval* a, char* op) {
  // ensure that arguments are numbers
  for (int i = 0; i < a->count; i++) {
    LASSERT_ARGT(op, a, i, LVAL_NUM);
  }

  // pop first element
  lval* x = lval_pop(a, 0);

  // if not arguments and sub then perform unary negation
  if ((strcmp(op, "-") == 0 ) && a->count == 0) {
    x->num = -x->num;
  }

  // while there are still elements remaining
  while (a->count > 0) {
    lval* y = lval_pop(a, 0);

    if (strcmp(op, "+") == 0) { x->num += y->num; }
    if (strcmp(op, "-") == 0) { x->num -= y->num; }
    if (strcmp(op, "*") == 0) { x->num *= y->num; }
    if (strcmp(op, "/") == 0 || strcmp(op, "%") == 0) {
      if (y->num == 0) {
        lval_del(x); lval_del(y);
        x = lval_err("division by zero"); break;
      }

      if (strcmp(op, "/") == 0) { x->num /= y->num; }
      else { x->num = (int) x->num % (int) y->num; }
    }
    if (strcmp(op, "^") == 0) { x->num = pow( x->num, y->num); }

    lval_del(y);
  }

  lval_del(a); return x;
}

// add
lval* builtin_add(lenv* e, lval* a) {
  return builtin_op(e, a, "+");
}

// sub
lval* builtin_sub(lenv* e, lval* a) {
  return builtin_op(e, a, "-");
}

// mul
lval* builtin_mul(lenv* e, lval* a) {
  return builtin_op(e, a, "*");
}

// div
lval* builtin_div(lenv* e, lval* a) {
  return builtin_op(e, a, "/");
}

// mod
lval* builtin_mod(lenv* e, lval* a) {
  return builtin_op(e, a, "%");
}

// pow
lval* builtin_pow(lenv* e, lval* a) {
  return builtin_op(e, a, "^");
}

// LIST FUNCTIONS
// head
lval* builtin_head(lenv* e, lval* a) {
  // check error conditions
  LASSERT_ARGC("head", a, 1);
  LASSERT_ARGT("head", a, 0, LVAL_QEXPR);
  LASSERT_ARGNE("head", a, 0);

  // otherwise take first argument
  lval* v = lval_take(a, 0);

  // delete all elements that are not head and return
  while (v->count > 1) { lval_del(lval_pop(v, 1)); }
  return v;
}

// tail
lval* builtin_tail(lenv* e, lval* a) {
  // error conditions
  LASSERT_ARGC("tail", a, 1);
  LASSERT_ARGT("tail", a, 0, LVAL_QEXPR);
  LASSERT_ARGNE("tail", a, 0);

  // take first argument
  lval* v = lval_take(a, 0);

  // delete firts element and return
  lval_del(lval_pop(v, 0));
  return v;
}

// join
lval* builtin_join(lenv* e, lval* a) {
  for (int i = 0; i < a->count; i++) {
    LASSERT_ARGT("join", a, i, LVAL_QEXPR);
  }

  lval* x = lval_pop(a, 0);

  while (a->count) {
    x = lval_join(x, lval_pop(a, 0));
  }

  lval_del(a);
  return x;
}

// list
lval* builtin_list(lenv* e, lval* a) {
  a->type = LVAL_QEXPR;
  return a;
}

// CONDITIONAL
// if {cond} {success} [{error}]
lval* builtin_if(lenv* e, lval* a) {
  LASSERT_ARGC_MIN("if", a, 2);
  LASSERT_ARGC_MAX("if", a, 3);

  // validate argument types
  LASSERT_ARGT("if", a, 0, LVAL_NUM);
  for (int i = 1; i < a->count; i++) {
    LASSERT_ARGT("if", a, i, LVAL_QEXPR);
    a->cell[i]->type = LVAL_SEXPR;
  }

  // eval
  lval* result = NULL;
  if (a->cell[0]->num) {
    result = lval_eval(e, lval_pop(a, 1));
  } else {
    if (a->count == 2) {
      result = lval_sexpr();
    } else {
      result = lval_eval(e, lval_pop(a, 2));
    }
  }

  // cleanup and return
  lval_del(a);
  return result;
}

// EVAL
// eval
lval* builtin_eval(lenv* e, lval* a) {
  LASSERT_ARGC("eval", a, 1);
  LASSERT_ARGT("eval", a, 0, LVAL_QEXPR);

  lval* x = lval_take(a, 0);
  x->type = LVAL_SEXPR;
  return lval_eval(e, x);
}

// load
lval* builtin_load(lenv* e, lval* a) {
  LASSERT_ARGC("load", a, 1);
  LASSERT_ARGT("load", a, 0, LVAL_STR);

  // open file and check it exists
  FILE* f = fopen(a->cell[0]->str, "rb");
  if (f == NULL) {
    lval* err = lval_err("Could not load Library %s", a->cell[0]->str);
    lval_del(a);
    return err;
  }

  fseek(f, 0, SEEK_END);
  long length = ftell(f);
  fseek(f, 0, SEEK_SET);
  char* input = calloc(length + 1, 1);
  fread(input, 1, length, f);
  fclose(f);

  // read from input to create an S-Expr
  int pos = 0;
  lval* expr = lval_parse(input, &pos, '\0');
  free(input);

  // evaluate all expressions contained in S-Expr
  if (expr->type != LVAL_ERR) {
    while (expr->count) {
      lval* x = lval_eval(e, lval_pop(expr, 0));
      if (x->type == LVAL_ERR) { lval_println(x); }
      lval_del(x);
    }
  } else {
    lval_println(expr);
  }

  lval_del(expr);
  lval_del(a);

  return lval_sexpr();
}

// print
lval* builtin_print(lenv* e, lval* a) {
  // print each argument followed by space
  for (int i = 0; i < a->count; i++) {
    lval_print(a->cell[i]); putchar(' ');
  }

  // print a newline and delete args
  putchar('\n');
  lval_del(a);

  return lval_sexpr();
}

lval* builtin_error(lenv* e, lval* a) {
  LASSERT_ARGC("error", a, 1);
  LASSERT_ARGT("error", a, 0, LVAL_STR);

  // construct error from first arg
  lval* err = lval_err(a->cell[0]->str);

  // cleanup and return
  lval_del(a);
  return err;
}

// printenv
lval* builtin_printenv(lenv* e, lval* a) {
  LASSERT_ARGC("printenv", a, 1);
  LASSERT_ARGT("printenv", a, 0, LVAL_QEXPR);
  LASSERT_ARGE("printenv", a, 0);
  for(int i = 0; i < e->count; i++) {
    printf("%s %s ", ltype_name(e->vals[i]->type), e->syms[i]);
    lval_print(e->vals[i]);
    putchar('\n');
  }
  return lval_sexpr();
}

// LENV
lenv* lenv_new(void) {
  lenv* e = malloc(sizeof(lenv));
  e->par = NULL;
  e->count = 0;
  e->syms = NULL;
  e->vals = NULL;
  return e;
}

// del
void lenv_del(lenv* e) {
  for (int i = 0; i < e->count; i++) {
    free(e->syms[i]);
    lval_del(e->vals[i]);
  }
  free(e->syms);
  free(e->vals);
  free(e);
}

// get
lval* lenv_get(lenv* e, lval* k) {
  // iterate over all env items
  for (int i = 0; i < e->count; i++) {
    // check if the stores string matches the symbol string
    // if it does, return a copy of the value
    if (strcmp(e->syms[i], k->sym) == 0) {
      return lval_copy(e->vals[i]);
    }
  }

  // if no symbol check in parent
  if (e->par) {
    return lenv_get(e->par, k);
  } else {
    return lval_err("Unbound Symbol '%s'", k->sym);
  }
}

// put
void lenv_put(lenv* e, lval* k, lval* v) {
  // iterate ovel all env items
  // check if symbol exists
  for (int i = 0; i < e->count; i++) {
    // if symbol is found delete item at that position
    // and replace with new value
    if (strcmp(e->syms[i], k->sym) == 0) {
      lval_del(e->vals[i]);
      e->vals[i] = lval_copy(v);
      return;
    }
  }

  // create instead
  e->count++;
  e->vals = realloc(e->vals, sizeof(lval*) * e->count);
  e->syms = realloc(e->syms, sizeof(char*) * e->count);

  // copy to new location
  e->vals[e->count - 1] = lval_copy(v);
  e->syms[e->count - 1] = malloc(strlen(k->sym) + 1);
  strcpy(e->syms[e->count - 1], k->sym);
}

void lenv_def(lenv* e, lval* k, lval* v) {
  // iterate till e has no parent
  while(e->par) { e = e->par; }
  // put value in e
  lenv_put(e, k, v);
}

// copy
lenv* lenv_copy(lenv* e) {
  lenv* n = malloc(sizeof(lenv));
  n->par = e->par;
  n->count = e->count;
  n->syms = malloc(sizeof(char*) * n->count);
  n->vals = malloc(sizeof(lval*) * n->count);
  for (int i = 0; i < n->count; i++) {
    n->syms[i] = malloc(strlen(e->syms[i]) + 1);
    strcpy(n->syms[i], e->syms[i]);
    n->vals[i] = lval_copy(e->vals[i]);
  }

  return n;
}

void lenv_add_builtin(lenv* e, char* name, lbuiltin func) {
  lval* k = lval_sym(name);
  lval* v = lval_fun(func);
  lenv_put(e, k, v);
  lval_del(k); lval_del(v);
}

// REGISTER BUILTIN
void lenv_add_builtins(lenv* e) {
  // list of functions
  lenv_add_builtin(e, "list", builtin_list);
  lenv_add_builtin(e, "head", builtin_head);
  lenv_add_builtin(e, "tail", builtin_tail);
  lenv_add_builtin(e, "join", builtin_join);

  // eval
  lenv_add_builtin(e, "eval", builtin_eval);

  // math functions
  lenv_add_builtin(e, "+", builtin_add);
  lenv_add_builtin(e, "-", builtin_sub);
  lenv_add_builtin(e, "*", builtin_mul);
  lenv_add_builtin(e, "/", builtin_div);
  lenv_add_builtin(e, "%", builtin_mod);
  lenv_add_builtin(e, "^", builtin_pow);

  // relational operators
  lenv_add_builtin(e, ">", builtin_gt);
  lenv_add_builtin(e, ">=", builtin_gte);
  lenv_add_builtin(e, "<", builtin_lt);
  lenv_add_builtin(e, "<=", builtin_lte);
  lenv_add_builtin(e, "==", builtin_eq);

  // variable functions
  lenv_add_builtin(e, "def", builtin_def);
  lenv_add_builtin(e, "=", builtin_put);
  lenv_add_builtin(e, "\\", builtin_lambda);

  // conditional functions
  lenv_add_builtin(e, "if", builtin_if);

  // string functions
  lenv_add_builtin(e, "load", builtin_load);
  lenv_add_builtin(e, "error", builtin_error);
  lenv_add_builtin(e, "print", builtin_print);

  // environment functions
  lenv_add_builtin(e, "printenv", builtin_printenv);
}

void load_file(lenv* e, char* filename) {
  lval* args = lval_add(lval_sexpr(), lval_str(filename));
  lval* x = builtin_load(e, args);
  if (x->type == LVAL_ERR) { lval_println(x); }
  lval_del(x);
}

void bootstrap(lenv* e) {
  load_file(e, "std.lspy");
}

int main (int argc, char** argv) {
  // ; number   : /-?[0-9]+\\.?[0-9]*/
  // ; symbol   : /[a-zA-Z0-9_+\\-*\\/\\\\=<>!&%\\^]+/
  // ; string   : /\"(\\\\.|[^\"])*\"/
  // ; comment  : /;[^\\r\\n]*/
  // ; sexpr    : '(' <expr>* ')'
  // ; qexpr    : '{' <expr>* '}'
  // ; expr     : <number>  | <symbol> | <string>
  // ;          | <comment> | <sexpr>  | <qexpr>
  // ; lispy    : /^/ <expr>* /$/

  // env
  lenv* e = lenv_new();
  lenv_add_builtins(e);

  // bootstrap
  bootstrap(e);

  // repl
  if (argc == 1) {
    puts("lisp version 0.0.1");
    puts("press ctrl+c to exit\n");

    while (1) {
      char* input = readline("> ");
      add_history(input);

      // read from input to create an S-Expr
      int pos = 0;
      lval* expr = lval_parse(input, &pos, '\0');

      // eval and print
      lval* x = lval_eval(e, expr);
      lval_println(x);

      free(input);
    }
  }

  // execute file
  if (argc >= 2) {
    // loop over each supplied filename
    for (int i = 1; i < argc; i++) {
      load_file(e, argv[i]);
    }
  }

  lenv_del(e);

  return 0;
}
